﻿/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

// Register a templates definition set named "default".
CKEDITOR.addTemplates( 'default', {
	// The name of sub folder which hold the shortcut preview images of the
	// templates.
		imagesPath: CKEDITOR.getUrl( CKEDITOR.plugins.getPath( 'templates' ) + 'templates/images/' ),

		// The templates definitions.
		templates: [
			{
				title: 'Page Section',
				image: 'template2.gif',
				description: 'Define a standard page section.',
				html:
							'	<div class="section">' +
							'		<div class="wrapper">' +
							'			Insert a column set' +
							'		</div>' +
							'	</div>'
			},
			{
				title: 'Page Section (alt)',
				image: 'template2.gif',
				description: 'Define an alternative page section.',
				html:
							'	<div class="section section-alt">' +
							'		<div class="wrapper">' +
							'			Insert a column set' +
							'		</div>' +
							'	</div>'
			},
			{
				title: '1 Columns (Full Width)',
				image: 'template2.gif',
				description: 'Full Width column wrapped in a row.',
				html:
							'	<div class="row">' +
							'		<div class="col-md-12">Replace this text with the content of your column</div>' +
							'	</div>'
			},
			{
				title: '2 Columns (6 / 6) ',
				image: 'template2.gif',
				description: '2 Columns of 50 % each wrapped in a row (6 cols in the grid each).',
				html:
							'	<div class="row">' +
							'		<div class="col-md-6">Replace this text with the content of your first column</div>' +
							'		<div class="col-md-6">Replace this text with the content of your second column</div>' +
							'	</div>'
			},
			{
				title: '3 Columns (4 / 4 / 4) ',
				image: 'template2.gif',
				description: '3 equal columns wrapped in a row (4 cols each).',
				html:
							'	<div class="row">' +
							'		<div class="col-md-4">Replace this text with the content of your first column</div>' +
							'		<div class="col-md-4">Replace this text with the content of your second column</div>' +
							'		<div class="col-md-4">Replace this text with the content of your third column</div>' +
							'	</div>'
			},
			{
				title: '4 Columns (3 / 3 / 3 / 3) ',
				image: 'template2.gif',
				description: '4 equal columns wrapped in a row (3 cols each).',
				html:
							'	<div class="row">' +
							'		<div class="col-md-3">Replace this text with the content of your first column</div>' +
							'		<div class="col-md-3">Replace this text with the content of your second column</div>' +
							'		<div class="col-md-3">Replace this text with the content of your third column</div>' +
							'		<div class="col-md-3">Replace this text with the content of your fourth column</div>' +
							'	</div>'
			},
			{
				title: 'Boxed Section',
				image: 'template2.gif',
				description: 'A template to define a fancy section.',
				html:
							'	<div class="box box-fancy">' +
		          '		<div class="box-content">' +
		          '			<div class="box-inner">' +
		          '				Insert your columns here' +
		          '			</div>' +
		          '		</div>' +
		          '	</div>'
			}
		]
} );
