"use strict"
/**
 * Dialog box to initialize the widgets values and options.
 *
 * Name: featuredbox dialog
 * Description: Choose the icon, the price values and the button options that
 *              will be used in the featuredbox
 *
 */
CKEDITOR.dialog.add( 'featuredbox', function( editor ) {
  // Create variable with values fetched from the languages files
  var lang = editor.lang.featuredbox,
  commonLang = editor.lang.common;

  return {
    title: 'Edit Featuredbox',    // Title of the popup window
    minWidth: 400,
    minHeight: 400,
    contents: [
      {
        id: 'info',               // Title of the window tab (if multiple)
        label: 'Image Info',
        accessKey: 'I',
        elements: [
          {
            type: 'vbox',
            padding: 0,
            children: [
              {
                type: 'hbox',
                widths: [ '280px', '110px' ],
                align: 'right',
                children: [
                  {
                    id: 'cloudicon',  // Name of the field
                    type: 'select',   // Field type
                    items:
                    [
                      ['standard cloud', 'i-cloud-servers-m'],
                      ['managed private cloud', 'i-managed-cloud-m'],
                      ['dedicated private cloud', 'i-private-cloud-m'],
                      ['hybrid cloud', 'i-hybrid-cloud-m']
                    ],
                    label: commonLang.urls,
                    setup: function( widget ) {
                      this.setValue( widget.data.cloudicon );
                    },
                    commit: function( widget ) {
                      widget.setData( 'cloudicon', this.getValue() );
                    },
                    validate: CKEDITOR.dialog.validate.notEmpty( 'Missing' )
                  },
                ]
              }
            ]
          },
          {
              // Create a new field in your popup window and set the associated
              // data in the widget setData.
              id: 'pricedollar',
              type: 'text',
              label: 'price dollar',
              setup: function( widget ) {
                this.setValue( widget.data.pricedollar );
              },
              commit: function( widget ) {
                widget.setData( 'pricedollar', this.getValue() );
              },
              validate: CKEDITOR.dialog.validate.notEmpty( 'Missing' )
          },
          {
              id: 'pricecent',
              type: 'text',
              label: 'price cent',
              setup: function( widget ) {
                this.setValue( widget.data.pricecent );
              },
              commit: function( widget ) {
                widget.setData( 'pricecent', this.getValue() );
              },
              validate: CKEDITOR.dialog.validate.notEmpty( 'Missing' )
          },
          {
              id: 'pricefrequency',
              type: 'text',
              label: 'price frequency',
              setup: function( widget ) {
                this.setValue( widget.data.pricefrequency );
              },
              commit: function( widget ) {
                widget.setData( 'pricefrequency', this.getValue() );
              },
              validate: CKEDITOR.dialog.validate.notEmpty( 'Missing' )
          },
          {
              id: 'divfeature',
              type: 'text',
              label: 'div feature',
              setup: function( widget ) {
                this.setValue( widget.data.divfeature );
              },
              commit: function( widget ) {
                widget.setData( 'divfeature', this.getValue() );
              },
              validate: CKEDITOR.dialog.validate.notEmpty( 'Missing' )
          },
          {
              id: 'buttontext',
              type: 'text',
              label: 'Button Text',
              setup: function( widget ) {
                this.setValue( widget.data.buttontext );
              },
              commit: function( widget ) {
                widget.setData( 'buttontext', this.getValue() );
              },
              validate: CKEDITOR.dialog.validate.notEmpty( 'Missing' )
          },
          {
              id: 'buttonstyle',
              type: 'text',
              label: 'Button Style',
              setup: function( widget ) {
                this.setValue( widget.data.buttonstyle );
              },
              commit: function( widget ) {
                widget.setData( 'buttonstyle', this.getValue() );
              },
              validate: CKEDITOR.dialog.validate.notEmpty( 'Missing' )
          },
        ]
      }
    ]
  };
} );
