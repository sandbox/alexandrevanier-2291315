"use strict";
/**
 * Register the plugin with CKEditor.
 *
 * Name: featuredbox
 * Description: Insert a preformatted content block with an icon, a title,
 *              a description, a price descrioption and a button.
 *
 */
CKEDITOR.plugins.add('featuredbox', {
  // Uses the language english.
  lang: 'en',

  // This plugin requires the Widgets and the dialog plugins.
  requires: 'widget,dialog',

  // Register the icon used for the toolbar button. It must be the same
  // as the name of the widget.
  icons: 'featuredbox',

  // The plugin logic and filtering goes inside this method.
  init: function(editor) {

    // Register the dialog file that will be used to configure our widget
    CKEDITOR.dialog.add('featuredbox', this.path + 'dialogs/featuredbox.js');

    // Register the featuredbox widget.
    editor.widgets.add('featuredbox', {
      //
      // Advanced Content Filter - defines the content rules of the widget
      // You can read more about ACF here:
      // http://docs.ckeditor.com/#!/guide/dev_advanced_content_filter
      // http://docs.ckeditor.com/#!/guide/plugin_sdk_integration_with_acf
      //
      // AllowedContent - Define which elements are allowed in the widget
      // (html elements, styles, classes, attributes, etc.)
      // All the other tags in your content will be removed automatically
      //
      allowedContent: {
        div: {
          classes: '!media-block,!media-content,!media-img,align-left,align-right,align-center',
        },
        i: true,
        h5: true,
        p: true,
      },

      // requiredContent - Define which elements are absolutely necessary
      // without theses, the widget won't work (normally the base tag)
      requiredContent: 'div(featuredbox)',

      // editables - Define the section in your template that will be editable
      // * if the section isn't defined here you won't be able to edit the
      // generated code added to the page by the widget.
      editables: {
        title: {
          // Define CSS selector used for finding the element inside widget element.
          selector: 'h5',
          // Define content allowed in this nested editable. Its content will be
          // filtered accordingly and the toolbar will be adjusted when this editable
          // is focused.
          allowedContent: '*'
        },
        content: {
          selector: 'p',
          allowedContent: '*'
        }
      },
      // list the "parts" of your widget and how they can be found
      parts: {
        divfeature: 'div.featuredbox',
        image: 'div.featuredbox i',
        pricedollar: 'span.price-dollar',
        pricecent: 'span.price-cent',
        pricefrequency: 'span.price-frequency',
        buttontext: 'a.btn',
        buttonstyle: 'a.btn',
      },

      // Define the HTML template of our featuredbox widget.
      // It will be used when creating new instances of the featuredbox.
      template:
              '<div class="featuredbox"> ' +
              '<i class="i-cloud-servers-m">&nbsp;</i>' +
              '<h5>' +
              '<a class="cloud" href="http://iweb.com/cloud/servers">' +
              'Public Cloud Servers' +
              '</a>' +
              '</h5>' +
              '<p>' +
              'Scalable OpenStack public cloud hosting for websites, applications and development.' +
              '</p>' +
              '<p class="pricing">' +
              '<span class="price-before">' +
              '<small>' +
              'From' +
              '</small>' +
              '</span>' +
              '<br />' +
              '<span class="price">' +
              '<span class="price-sign">' +
              '$' +
              '</span>' +
              '<span class="price-dollar">' +
              '0' +
              '</span>' +
              '<span class="hidden">' +
              '.' +
              '</span>' +
              '<span class="price-cent">' +
              '06' +
              '</span>' +
              '<span class="price-frequency">' +
              '/hour' +
              '</span>' +
              '</span>' +
              '</p>' +
              '<p>' +
              '<a class="btn cloud" href="http://iweb.com/cloud/servers">' +
              'Choose Server' +
              '</a>' +
              '</p>' +
              '</div>',
      // Define the label for the widget button in the editor
      // Note: If you want to translate your widget you should use the
      // editor.lang.plugin_name.* property instead of a standard string.
      button: 'Create a featuredbox',

      // Name of the dialog that you call in order to initialize the widget
      dialog: 'featuredbox',

      // Check the elements that need to be converted to widgets.
      //
      // Note: The "element" argument is an instance of http://docs.ckeditor.com/#!/api/CKEDITOR.htmlParser.element
      // so it is not a real DOM element yet. This is caused by the fact that upcasting is performed
      // during data processing which is done on DOM represented by JavaScript objects.
      upcast: function(element) {
        return element.name == 'div' && element.hasClass('featuredbox');
      },

      // When a widget is being initialized, we need to read the data
      // from DOM and set it by using the widget.setData() method.
      // More code which needs to be executed when DOM is available may go here.
      init: function() {
        this.setData('cloudicon', this.parts.image.getAttribute('class') || '');
        this.setData('divfeature', this.parts.divfeature.getAttribute('class') || '');
        this.setData('pricedollar', this.parts.pricedollar.getText() || '');
        this.setData('pricecent', this.parts.pricecent.getText() || '');
        this.setData('pricefrequency', this.parts.pricefrequency.getText() || '');
        this.setData('buttontext', this.parts.buttontext.getText() || '');
        this.setData('buttonstyle', this.parts.buttonstyle.getAttribute('class') || '');
      },

      // Listen on the widget#data event which is fired every time the widget data changes
      // and updates the widget's view.
      // Data may be changed by using the widget.setData() method, which we will
      // use in the more complex widget dialog window.
      data: function() {
        this.parts.image.setAttribute('class', this.data.cloudicon);
        this.parts.divfeature.setAttribute('class', this.data.divfeature);
        this.parts.pricedollar.setText(this.data.pricedollar);
        this.parts.pricecent.setText(this.data.pricecent);
        this.parts.pricefrequency.setText(this.data.pricefrequency);
        this.parts.buttontext.setText(this.data.buttontext);
        this.parts.buttonstyle.setAttribute('class', this.data.buttonstyle);
      }
    });
  }
});

