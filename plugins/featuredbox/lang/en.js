"use strict"

CKEDITOR.plugins.setLang( 'featuredbox', 'en', {
  alt: 'Alternative Text',
  alignTitle: 'Align Image',
  infoTab: 'Image Info',
  linkTitle: 'Image Link',
} );
