/**
 * Register the plugin with CKEditor.
 *
 * Name: section_title
 * Description: Insert a preformatted content block with a centered <h3> tag
 *              for a section title and a <p> tag for the section description
 *
 */
CKEDITOR.plugins.add( 'section_title', {
  // This plugin requires the Widgets System defined in the 'widget' plugin.
  requires: 'widget',

  // Register the icon used for the toolbar button. It must be the same
  // as the name of the widget.
  icons: 'section_title',

  // The plugin logic and filtering goes inside this method.
  init: function( editor ) {

    // Register the section_title widget.
    editor.widgets.add( 'section_title', {
      //
      // Advanced Content Filter - defines the content rules of the widget
      // You can read more about ACF here:
      // http://docs.ckeditor.com/#!/guide/dev_advanced_content_filter
      // http://docs.ckeditor.com/#!/guide/plugin_sdk_integration_with_acf
      //
      // AllowedContent - Define which elements are allowed in the widget
      // (html elements, styles, classes, attributes, etc.)
      // All the other tags in your content will be removed automatically
      //
      allowedContent: {
        div: {                         // name of the base selector
          classes: '!section_title',   // ! means that the class is required
        },
        p: {
          classes: '!lead',            // which class is allowed with a p
        },
        p: true,                       // syntax to allow a tag without condition
        h3: true,
      },

      // requiredContent - Define which elements are absolutely necessary
      // without theses, the widget won't work (normally the base tag)
      requiredContent: 'div(section_title)',

      // editables - Define the section in your template that will be editable
      // * if the section isn't defined here you won't be able to edit the
      // generated code added to the page by the widget.
      editables: {
        title: {                        // Name of the editable section
          // Define the CSS selector used for finding the element.
          selector: 'h3',
          //
          // you could add another allowedContent section if you want to add
          // specific content rules to this section.
          // exemple - allowedContent: 'br strong em span(text-muted)'
        },
        content: {
          selector: '.lead',
       }
      },

      // list the "parts" of your widget and how they can be found
      parts: {
        title: 'h3',
        content: '.lead'
      },

      // Define the HTML template of our section title widget.
      // It will be used when creating new instances of the section title block.
      template:
        '<div class="section_title">'+
          '<h3 class="text-center">' +
            'OpenStack cloud hosting with the stability of dedicated servers' +
          '</h3>' +
          '<p class="lead text-center">' +
            'The iWeb Cloud is a public cloud powered by OpenStack,' +
            'open source cloud computing with over 12,000 developers worldwide.' +
            'SolidFire SSD Storage fuels your servers with Burstable IOPS,' +
            'for guaranteed performance stability even when workloads spike.' +
          '</p>' +
        '</div>',

      // Define the label for the widget button in the editor
      // Note: If you want to translate your widget you should use the
      // editor.lang.plugin_name.* property instead of a standard string.
      button: 'Create a section title',

      // Check the elements that need to be converted to widgets.
      //
      // Note: The "element" argument is an instance of http://docs.ckeditor.com/#!/api/CKEDITOR.htmlParser.element
      // so it is not a real DOM element yet. This is caused by the fact that upcasting is performed
      // during data processing which is done on DOM represented by JavaScript objects.
      upcast: function( element ) {
        // Return "true" (that element needs to converted to a section_title widget)
        // for all <div> elements with a "section_title" class.
        return element.name == 'div' && element.hasClass( 'section_title' );
      },

      // When a widget is being initialized, we need to read the data
      // from DOM and set it by using the widget.setData() method.
      // More code which needs to be executed when DOM is available may go here.
      init: function() {
      },

      // Listen on the widget#data event which is fired every time the widget data changes
      // and updates the widget's view.
      // Data may be changed by using the widget.setData() method, which we will
      // use in the more complex widget dialog window.
      data: function() {
      }
    } );
  }
} );
