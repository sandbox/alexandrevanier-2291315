"use strict";
/**
 * Register the plugin with CKEditor.
 *
 * Name: mediabox
 * Description: Insert a preformatted content block with an icon and a text block
 *
 */
CKEDITOR.plugins.add('mediabox', {

  // This plugin requires the Widgets and the dialog plugins.
  requires: 'widget,dialog',

  // Register the icon used for the toolbar button. It must be the same
  // as the name of the widget.
  icons: 'mediabox',

  // The plugin logic and filtering goes inside this method.
  init: function(editor) {

    // Register the dialog file that will be used to configure our widget
    CKEDITOR.dialog.add('mediabox', this.path + 'dialogs/mediabox.js');

    // Register the mediabox widget.
    editor.widgets.add('mediabox', {
      //
      // Advanced Content Filter - defines the content rules of the widget
      // You can read more about ACF here:
      // http://docs.ckeditor.com/#!/guide/dev_advanced_content_filter
      // http://docs.ckeditor.com/#!/guide/plugin_sdk_integration_with_acf
      //
      // AllowedContent - Define which elements are allowed in the widget
      // (html elements, styles, classes, attributes, etc.)
      // All the other tags in your content will be removed automatically
      //
      allowedContent: {
        div: {
          classes: '!media-block,!media-content,!media-img,align-left,align-right,align-center',
        },
        i: true,
        h5: true,
        p: true,
      },

      // requiredContent - Define which elements are absolutely necessary
      // without theses, the widget won't work (normally the base tag)
      requiredContent: 'div(media-block)',

      // editables - Define the section in your template that will be editable
      // * if the section isn't defined here you won't be able to edit the
      // generated code added to the page by the widget.
      editables: {
        title: {
          // Define CSS selector used for finding the element inside widget element.
          selector: 'h5',
          // Define content allowed in this nested editable. Its content will be
          // filtered accordingly and the toolbar will be adjusted when this editable
          // is focused.
          allowedContent: 'br strong em span(text-muted)'
        },
        content: {
          selector: 'p',
          allowedContent: 'p br ul ol li strong em a i'
        }
      },

      // list the "parts" of your widget and how they can be found
      parts: {
        colImage: 'div.media-img',
        colText: 'div.media-content',
        image: 'div.media-img i',
        h2: 'div.media-content h5',
        content: 'div.media-content p'
      },

      // Define the HTML template of our mediabox widget.
      // It will be used when creating new instances of the mediabox.
      template:
              '<div class="media-block">' +
                '<div class="media-img">' +
                  '<i class="icon-timer">' +
                    '&nbsp;' +
                  '</i>' +
                '</div>' +
                '<div class="media-content">' +
                  '<h5>' +
                    'Fast deployment' +
                  '</h5>' +
                  '<p>' +
                    'Fully-configured and live from just 15 minutes' +
                  '</p>' +
              '</div>' +
              '</div>',

      // Define the label for the widget button in the editor
      // Note: If you want to translate your widget you should use the
      // editor.lang.plugin_name.* property instead of a standard string.
      button: 'Create a mediabox',

      // Name of the dialog that you call in order to initialize the widget
      dialog: 'mediabox',

      // Check the elements that need to be converted to widgets.
      //
      // Note: The "element" argument is an instance of http://docs.ckeditor.com/#!/api/CKEDITOR.htmlParser.element
      // so it is not a real DOM element yet. This is caused by the fact that upcasting is performed
      // during data processing which is done on DOM represented by JavaScript objects.
      upcast: function(element) {
        return element.name == 'div' && element.hasClass('media-block');
      },

      // When a widget is being initialized, we need to read the data
      // from DOM and set it by using the widget.setData() method.
      // More code which needs to be executed when DOM is available may go here.
      init: function() {
        this.setData('iconclass', this.parts.image.getAttribute('class') || '');
      },

      // Listen on the widget#data event which is fired every time the widget data changes
      // and updates the widget's view.
      // Data may be changed by using the widget.setData() method, which we will
      // use in the more complex widget dialog window.
      data: function() {
        this.parts.image.setAttribute('class', this.data.iconclass);
      }
    });
  }
});
