"use strict"

CKEDITOR.plugins.setLang( 'mediabox', 'en', {
  alt: 'Alternative Text',
  alignTitle: 'Align Image',
  infoTab: 'Image Info',
  linkTitle: 'Image Link',
} );
