"use strict"
/**
 * Dialog box to initialize the widgets values and options.
 *
 * Name: mediabox dialog
 * Description: Choose the icon that will be used with the mediabox
 *
 */
CKEDITOR.dialog.add( 'mediabox', function( editor ) {
  // Create variable with values fetched from the languages files
  var lang = editor.lang.mediabox,
  commonLang = editor.lang.common;

  return {
    title: 'Edit Mediabox',   // Title of the popup window
    minWidth: 400,
    minHeight: 300,
    contents: [
      {
        id: 'info',           // Title of the window tab (if multiple)
        label: 'info tab',
        accessKey: 'I',
        elements: [
          {
            type: 'vbox',
            padding: 0,
            children: [
              {
                type: 'hbox',
                widths: [ '280px', '110px' ],
                align: 'right',
                children: [
                  {
                    // Create a field in the pop up window
                    id: 'iconclass', // name of the field (used in plugin.js)
                    type: 'select',  // type of the input
                    items:
                    [
                      [ 'Default', '' ],
                      [ 'Christmas tree', 'glyphicon glyphicon-tree-conifer' ],
                      [ 'Saved', 'glyphicon glyphicon-saved' ],
                      [ 'Lightning', 'glyphicon glyphicon-flash' ],
                      [ 'Heart', 'glyphicon glyphicon-heart-empty' ]
                    ],
                    label: 'icon class',
                    setup: function( widget ) {
                      this.setValue( widget.data.iconclass );
                    },
                    commit: function( widget ) {
                      widget.setData( 'iconclass', this.getValue() );
                    },
                    validate: CKEDITOR.dialog.validate.notEmpty( 'urls missing' )
                  },
                ]
              }
            ]
          },
        ]
      }
    ]
  };
} );
