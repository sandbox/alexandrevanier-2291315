// $Id:$

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Additional Information
 * Customize and add new features
 * Troubleshooting
 * FAQ


INTRODUCTION
------------

Current Maintainer: Alexandre Vanier <avanier@iweb.com>

This module offer an extension of functionnalities to the CKEditor module.
It provides a way to quickly build popular page elements and structure directly
in the editor.

The module workflow is as follows:

  - Add the page elements you want to your editor in the CKEditor profile
  - Create a new content element
  - Use the modified template plugin to define the page layout (row, cols, etc.)
  - Fill the content sections with preformatted page elements widgets
  - Hit Save
  - Done!

This module is meant to speed up the workflow of building similar pages in a
webiste. With this new worflow, you can create complex pages without having to
rely on Drupal modules like panels or blocks. The elements can all be easily
modified in order to suit your website either by changing the module css file or
by changing the html templates of the page widgets.

INSTALLATION
------------

In order to install and use the complete workflow of this module, you need to
follow the following steps:

1. Download the CKEditor module from https://www.drupal.org/project/ckeditor
2. Download the libraries module (recommended)
3. Download the jQuery Update module
4. Download this module (page_builder) from Drupal or with the tar.gz
5. Enable all the modules
6. Go to the CkEditor Website http://ckeditor.com/
      7.1 Go to the addons section
      7.2 Click on the build my editor
      7.3 Choose a full version and be sure to add the "Widget" plugin
      7.4 Download it
7. Go to the Admin -> Config -> Development -> jQuery and set the default to 1.7
8. Go to the Admin -> Config -> Content Authoring -> Ckeditor
      8.1 In the Global Settings profile, change the Path to CKEditor to %l/ckeditor
      8.2 Go back and enter the specific profile of your choice (ex. Full)
      8.3 Add config.allowedContent = true; in the Custom JavaScript -> Custom config

Set-up your editor page builder tools.

1. Go to Admin -> Config -> Content Authoring -> Ckeditor -> your profile (Full)
2. Edit your profile -> go to the bottom of the page in the plugins section
3. Choose the different page builder plugins that you want to enable
4. Go back to the toolbar section at the top and drag the enabled plugins in your bar
5. Save

Done!

//////////////
// Optional //
//////////////

If you want to have a better experience in the editor we highly suggest that
you apply the same style used in the plugin (or your theme) to your editor

In order to do that you need to:

1. Go to the CSS tab of your profile and point to the plugin css file (css file path)

ADDITIONAL INFORMATION
-----------------------------------------

You can find a lot of usefull informations to help you understand and customize
this module at the following urls.

https://www.drupal.org/project/ckeditor
http://ckeditor.com/
http://docs.ckeditor.com/#!/guide/dev_advanced_content_filter


CUSTOMIZE AND ADD NEW FEATURES
-----------------------------------------

// Upcoming


TROUBLESHOOTING
-----------------------------------------

// Upcoming

FAQ
-----------------------------------------

// Upcoming



CONTACT
-----------------------------------------

Alexandre Vanier
<developer>
avanier@iweb.com
vanier.alexandre@gmail.com


